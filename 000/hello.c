#include <linux/init.h>
#include <linux/module.h>

MODULE_LICENSE("Dual BSD/GPL");

static int __init init_hello(void) {
	pr_info("Hello Module!!\n");
	return 0;
}

static void __exit exit_hello(void) {
	pr_info("Exit Module!!\n");
}

module_init(init_hello);
module_exit(exit_hello);
