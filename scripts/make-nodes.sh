#!/bin/bash

module="scull"
device="scull"
maxdevs="3"

set -e

cd $1
make
insmod $module.ko

major=$(grep -i scull < /proc/devices | awk '{print $1}')

rm -rf /dev/$device/[0-$maxdevs]

for i in $(seq 0 $maxdevs); do
    mknod /dev/${device}$i c $major $i
    chmod 777 /dev/${device}$i
done
