#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

apt-get -y dist-upgrade
apt-get -y install build-essential
apt-get -y install linux-source linux-headers-amd64
apt-get -y install libncurses-dev
apt-get -y install fish neovim htop git tmux ntp

cd /vagrant
version_fragment=$(uname -r | grep -Po "([0-9]+\.[0-9]+)")
apt_version_fragment=$(uname -r | grep -Po "[0-9]+\.[0-9]+\.[0-9]+-[0-9]")
debian_kernel_dir="$(pwd)/linux-source-${version_fragment}"
mainline_kernel_dir="$(pwd)/linux"

if [[ ! -d $debian_kernel_dir ]]; then
    tar -xJf "/usr/src/linux-source-${version_fragment}.tar.xz"
fi
