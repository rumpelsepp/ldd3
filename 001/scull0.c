#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/mutex.h>

#include "scull.h"

int scull_major = SCULL_MAJOR;
int scull_minor = 0;
int scull_no_devs = SCULL_NO_DEVS;
int scull_quantum = SCULL_QUANTUM;
int scull_qset = SCULL_QSET;
struct scull_dev *scull_devs;

module_param(scull_major, int, S_IRUGO);
module_param(scull_minor, int, S_IRUGO);
module_param(scull_no_devs, int, S_IRUGO);
module_param(scull_quantum, int, S_IRUGO);
module_param(scull_qset, int, S_IRUGO);

MODULE_AUTHOR("Stefan Tatschner");
MODULE_LICENSE("Dual BSD/GPL");
#define MODULE_NAME "SCULL0"

int scull_open(struct inode *inode, struct file *filep) {
	struct scull_dev *dev;

	// cdev is initialized within struct scull_dev.
	// So, in memory, cdev is wrapped by an instance of
	// struct scull_dev => it is possible to get scull_dev
	// when a pointer to the active cdev instance is available.
	dev = container_of(inode->i_cdev, struct scull_dev, cdev);
	filep->private_data = dev;

	if ((filep->f_flags & O_ACCMODE) == O_WRONLY) {
		if (mutex_lock_interruptible(&dev->mut)) {
			return -ERESTARTSYS;
		}

		scull_trim(dev);

		mutex_unlock(&dev->mut);
	}

	return 0;
}

ssize_t scull_read(struct file *filep, char __user *buf, size_t count, loff_t *offp) {
	struct scull_dev *dev = filep->private_data;
	struct scull_qset *dataptr;
	int qsetsize = dev->quantum * dev->qset;
	ssize_t err = 0;
	int qsetindex;
	int qsetoffset;
	int quantumindex;
	int quantumoffset;

	if (mutex_lock_interruptible(&dev->mut)) {
		return -ERESTARTSYS;
	}

	if (*offp >= dev->size) {
		goto out;
	}

	if (dev->size + count > dev->size) {
		count = dev->size - *offp;
	}

	qsetindex = *offp / qsetsize;
	qsetoffset = *offp % qsetsize;
	quantumindex = qsetoffset / dev->quantum;
	quantumoffset = qsetoffset % dev->quantum;

	dataptr = scull_follow(dev, qsetindex);

	if (!dataptr || !dataptr->data || !dataptr->data[quantumoffset]) {
		goto out;
	}

	if (count + quantumoffset > dev->quantum) {
		count = dev->quantum - quantumoffset;
	}

	err = copy_to_user(buf, dataptr->data[quantumindex] + quantumoffset, count);

	if (err) {
		err = -EFAULT;
		goto out;
	}

	*offp += count;
	err = count;

out:
	mutex_unlock(&dev->mut);

	return err;
}

ssize_t scull_write(struct file *filep, const char __user *buf, size_t count, loff_t *offp) {
	struct scull_dev *dev = filep->private_data;
	struct scull_qset *dataptr;
	int qsetsize = dev->quantum * dev->qset;
	ssize_t err = -ENOMEM;
	int qsetindex;
	int qsetoffset;
	int quantumindex;
	int quantumoffset;

	if (mutex_lock_interruptible(&dev->mut)) {
		return -ERESTARTSYS;
	}

	qsetindex = *offp / qsetsize;
	qsetoffset = *offp % qsetsize;
	quantumindex = qsetoffset / dev->quantum;
	quantumoffset = qsetoffset % dev->quantum;

	dataptr = scull_follow(dev, qsetindex);

	if (!dataptr) {
		goto out;
	}

	if (!dataptr->data) {
		dataptr->data = kzalloc(dev->qset * sizeof(char *), GFP_KERNEL);
		if (!dataptr->data) {
			goto out;
		}
	}

	if (!dataptr->data[quantumindex]) {
		dataptr->data[quantumindex] = kzalloc(dev->quantum, GFP_KERNEL);
		if (!dataptr->data[quantumindex]) {
			goto out;
		}
	}

	if (quantumoffset + count > dev->quantum) {
		count = dev->quantum - quantumoffset;
	}

	err = copy_from_user(dataptr->data[quantumindex] + quantumoffset, buf, count);

	if (err) {
		err = -EFAULT;
		goto out;
	}

	*offp += count;
	err = count;

	if (dev->size < *offp) {
		dev->size = *offp;
	}

out:
	mutex_unlock(&dev->mut);

	return err;
}

int scull_release(struct inode *inode, struct file *filep) {
	return 0;
}

struct file_operations scull_fops = {
	.owner		= THIS_MODULE,
	.open		= scull_open,
	.read		= scull_read,
	.write		= scull_write,
	.release	= scull_release,
};

static void scull_setup_cdev(struct scull_dev *dev, int index) {
	int err;
	dev_t devno = MKDEV(scull_major, scull_minor + index);

	cdev_init(&dev->cdev, &scull_fops);
	dev->cdev.owner = THIS_MODULE;
	dev->cdev.ops = &scull_fops;
	err = cdev_add(&dev->cdev, devno, 1);
	if (err) {
		pr_err("Error %d adding char dev scull%d\n", err, index);
	}
}

static void scull_exit_module(void) {
	int i = 0;
	dev_t devno = MKDEV(scull_major, scull_minor);

	pr_info("Exit %s\n", MODULE_NAME);

	if (scull_devs) {
		for (i = 0; i <= scull_no_devs; i++) {
			scull_trim(&scull_devs[i]);
			cdev_del(&scull_devs[i].cdev);
		}

		kfree(scull_devs);
	}

	unregister_chrdev_region(devno, scull_no_devs);

	// scull_p_cleanup();
	// scull_access_cleanup();
}

static int __init scull_init_module(void) {
	dev_t devno;
	int err = 0;
	int i = 0;

	pr_info("Hello, this is %s\n", MODULE_NAME);

	if (scull_major) {
		devno = MKDEV(scull_major, scull_minor);
		err = register_chrdev_region(devno, scull_no_devs, MODULE_NAME);
	} else {
		err = alloc_chrdev_region(&devno, scull_minor, scull_no_devs, MODULE_NAME);
		scull_major = MAJOR(devno);
	}
	if (err < 0) {
		pr_err("Cannot allocate chrdev region");
		return err;
	}

	scull_devs = kzalloc(scull_no_devs * sizeof(struct scull_dev), GFP_KERNEL);
	if (!scull_devs) {
		err = -ENOMEM;
		goto fail;
	}

	for (i = 0; i <= scull_no_devs; i++) {
		scull_devs[i].quantum = scull_quantum;
		scull_devs[i].qset = scull_qset;
		mutex_init(&scull_devs[i].mut);
		scull_setup_cdev(&scull_devs[i], i);
	}

	return 0;

fail:
	scull_exit_module();
	return err;
}

module_init(scull_init_module);
module_exit(scull_exit_module);
