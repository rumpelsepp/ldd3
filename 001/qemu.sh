#!/bin/bash

kvm -kernel $HOME/Projects/vendor/linux-stable/arch/x86/boot/bzImage \
    -drive file=$HOME/sid-amd64.img,media=disk,format=raw,readonly=off \
    -m 512M \
    -cpu host \
    -append "root=/dev/sda console=ttyS0" \
    -fsdev local,id=fs1,path=$HOME/Projects,security_model=none \
    -device virtio-9p-pci,fsdev=fs1,mount_tag=host-projects \
    -nographic
