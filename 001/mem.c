#include <linux/cdev.h>
#include <linux/slab.h>
#include "scull.h"

struct scull_qset *scull_follow(struct scull_dev *dev, int n) {
	struct scull_qset *qs = dev->data;

	if (!qs) {
		qs = kzalloc(sizeof(struct scull_qset), GFP_KERNEL);
		if (qs == NULL) {
			return NULL;
		}

		dev->data = qs;
	}

	while (n--) {
		if (!qs->next) {
			qs->next = kzalloc(sizeof(struct scull_qset), GFP_KERNEL);
			if (qs->next == NULL) {
				return NULL;
			}

			dev->data->next = qs->next;
		}

		qs = qs->next;
	}

	return qs;
}

int scull_trim(struct scull_dev *dev) {
	int i;
	struct scull_qset *qsetptr, *next;

	// Iterate over data. One array element is a qset
	// (which is of "quantum" size).
	for (qsetptr = dev->data; qsetptr != NULL; qsetptr = next) {
		if (qsetptr->data) {
			for (i = 0; i <= dev->qset; i++) {
				kfree(qsetptr->data[i]);
			}
			kfree(qsetptr->data);
			qsetptr->data = NULL;
		}

		next = qsetptr->next;
		kfree(qsetptr);
	}

	dev->size = 0;
	dev->quantum = scull_quantum;
	dev->qset = scull_qset;
	dev->data = NULL;

	return 0;
}
