#!/bin/bash
 
set -e
 
FS="ext4"
ARCH="amd64"
DEBIAN_DISTRO="sid"
MOUNT_POINT="/tmp/${DEBIAN_DISTRO}/"
IMG="/home/stefan/$DEBIAN_DISTRO-$ARCH.img"
IMGSIZE="1G"
 
if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root" 1>&2
    exit 1
fi
 
# Check if the disk image is already mounted, 
# if that's the case, then unmount it.
mountpoint -q $MOUNT_POINT && umount -f $MOUNT_POINT
 
echo "Creating disk image $IMG"
fallocate -l $IMGSIZE $IMG
du -h $IMG
 
echo "Creating filesystem $FS on $IMG"
mkfs.$FS -F $IMG
file $IMG
 
echo "Creating mountpoint $MOUNT_POINT"
mkdir -p $MOUNT_POINT
echo "Mounting $IMG, mountpoint: $MOUNT_POINT"
mount -o loop $IMG $MOUNT_POINT
 
echo "Creating the root file system"
cdebootstrap --arch=$ARCH \
    --include="fish,neovim,tmux,htop,build-essential" \
    $DEBIAN_DISTRO \
    $MOUNT_POINT \
    "https://deb.debian.org/debian"

echo "Applying some configuration"
sed -i '/^root/ { s/:x:/::/ }' $MOUNT_POINT/etc/passwd
echo "/dev/sda / $FS errors=remount-ro 0 1" >> $MOUNT_POINT/etc/fstab
echo "host-projects /mnt/host 9p  0 0"      >> $MOUNT_POINT/etc/fstab
echo "auto enp0s3"            >> $MOUNT_POINT/etc/network/interfaces
echo "iface enp0s3 inet dhcp" >> $MOUNT_POINT/etc/network/interfaces
echo "exec fish" >> $MOUNT_POINT/root/.bashrc
mkdir -p $MOUNT_POINT/mnt/host
 
echo "Cleaning up"
umount $MOUNT_POINT
rm -rf $MOUNT_POINT
 
echo "image '$IMG' created"
