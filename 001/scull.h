#pragma once

#include <linux/mutex.h>

#ifndef SCULL_MAJOR
#define SCULL_MAJOR	0
#endif

#ifndef SCULL_NO_DEVS
#define SCULL_NO_DEVS	4
#endif

#ifndef SCULL_QUANTUM
#define SCULL_QUANTUM	4096
#endif

#ifndef SCULL_QSET
#define SCULL_QSET	1024
#endif

extern int scull_quantum;
extern int scull_qset;

struct scull_dev {
	struct scull_qset *data;	// pointer to first quantum set
	int quantum;			// current quantum size
	int qset;			// current array size
	unsigned long size;		// amount of stored data
	unsigned int access_key;	// used by sculluid and scullpriv
	struct mutex mut;		// mutual exclusion semaphore
	struct cdev cdev;		// char device structure
};

struct scull_qset {
	void **data;
	struct scull_qset *next;
};

int scull_trim(struct scull_dev *);
struct scull_qset *scull_follow(struct scull_dev *, int);
